var unirest = require("unirest");
var jsonfile = require('jsonfile');
const winston = require('winston');
const fs = require('fs');
const logDir = 'log';
var filePath = __dirname + '\\assets' + '\\assets.json';
var dataObject = new Date();
var hours = dataObject.getHours();
var minutes = dataObject.getMinutes();
//timestamp = timestamp.replace(' ', '');
const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = new(winston.Logger)({
    transports: [
        // colorize the output to the console
        new(winston.transports.Console)({
            timestamp: tsFormat,
            colorize: true,
            level: 'debug'
        }),
        new(winston.transports.File)({
            //filename: logDir + '/results.log',
            filename: __dirname + '\\' + logDir + '\\results' + '_' + hours + '_' + minutes + '.log',
            json: false,
            timestamp: tsFormat,
            level: 'debug'
        })
    ]
});

var elementKey;
var secretUser;
var secretOrganization;
var userOrganizationSecret;
var randomChars = Math.random().toString(36).substring(3, 8);
var elementName;
var hub;
var secretElement;
var elementUserOrganizationSecret;


function checkFile() {
    return new Promise(function(resolve, reject) {
        return jsonfile.readFile(filePath, function(err, fileObject) {
            if (fileObject === undefined) {
                //logger.log('error', '%j', err);
                reject(err);
            } else {
                //logger.log('info', '%j', fileObject);
                logger.log('debug', 'Entries of assets.json ');
                logger.log('debug', '%j', fileObject);
                elementKey = fileObject.elementKey ? fileObject.elementKey : '';
                secretUser = fileObject.secretUser ? fileObject.secretUser : '';
                secretOrganization = fileObject.secretOrganization ? fileObject.secretOrganization : '';
                userOrganizationSecret = 'User ' + secretUser + ',' + ' Organization ' + secretOrganization;
                resolve(fileObject);
            }
        })
    })
}

function checkElement() {
    return new Promise(function(resolve, reject) {
        var findInstance = unirest("GET", "https://staging.cloud-elements.com/elements/api-v2/instances/" + elementKey);
        findInstance.headers({
            "authorization": userOrganizationSecret
        });
        return findInstance.end(function(findInstanceRes) {
            if (findInstanceRes.error && findInstanceRes.error.status !== 404) {
                logger.log('error', 'Error while validation Please verify assets.json');
                logger.log('error', findInstanceRes.error);
                logger.log('error', findInstanceRes.body)
                reject(findInstanceRes.error);
            } else {
                if (findInstanceRes.body && findInstanceRes.body.message && findInstanceRes.body.message.includes('No element')) {
                    logger.log('error', 'Error while validation Please verify assets.json');
                    logger.log('error', findInstanceRes.body)
                    logger.log('error', findInstanceRes.body.message);
                    reject(findInstanceRes.body.message);
                } else {
                    logger.log('info', 'assets.json is valid');
                    elementName = findInstanceRes.body.element.key;
                    hub = findInstanceRes.body.element.hub;
                    secretElement = findInstanceRes.body.token;
                    elementUserOrganizationSecret = 'Element ' + secretElement + ', ' + userOrganizationSecret;
                    resolve({});
                }
            }
        });
    });
}

function getApplicableTransformation() {
    return new Promise(function(resolve, reject) {
        var applicableTranReq = unirest("GET", 'https://staging.cloud-elements.com/elements/api-v2/elements/' + elementName + '/transformations');
        applicableTranReq.headers({
            "authorization": userOrganizationSecret
        });
        return applicableTranReq.end(function(applicableTranRes) {
            if (applicableTranRes.error && applicableTranRes.error.status !== 404) {
                logger.log('error', 'Something went wrong while getting transformations elementName' + elementName);
                logger.log('error', applicableTranRes.error);
                reject(applicableTranRes.error);
            } else {
                if (applicableTranRes.body && applicableTranRes.body.message && (applicableTranRes.body.message.includes('No element') || applicableTranRes.body.message.includes('No default transformation'))) {
                    logger.log('error', 'Something went wrong while getting transformations elementName' + elementName);
                    logger.log('error', applicableTranRes.body.message);
                    reject(applicableTranRes.body.message);
                } else {
                    resolve(applicableTranRes.body);
                }
            }
        });
    });
}

function getFileObjectForEachTransformation(transformationData) {
    return new Promise(function(resolve, reject) {
        transformationData.forEach((singleTransformationData) => {
            if (singleTransformationData) {
                var useDefaultTransformation = true;
                var singleTransformationDataVendorName = singleTransformationData.vendorName;
                var singleTransformationDataName = singleTransformationData.name;
                var fileNameBulk = __dirname + '\\elements\\' + elementName + '\\assets\\' + singleTransformationDataName + '.json';
                jsonfile.readFile(fileNameBulk, function(err, fileObjectBulk) {
                    logger.log('info', '****************** ' + singleTransformationDataVendorName + ' ******************');
                    logger.log('info', 'Searching for file : ' + fileNameBulk);
                    if (fileObjectBulk === undefined) {
                        // will use default transformation
                        logger.log('error', 'Cannot find : ' + singleTransformationDataName + '.json' + ' or it is invalid');
                        logger.log('error', err);
                        logger.log('info', 'Making defualt transformation for ' + singleTransformationDataName);
                    } else {
                        if (fileObjectBulk) {
                            logger.log('debug', 'file data : ');
                            logger.log('debug', '%j', fileObjectBulk);
                            if (typeof fileObjectBulk === 'object') {
                                useDefaultTransformation = Object.keys(fileObjectBulk).some((insideKey) => {
                                    //return typeof fileNameBulk[insideKey] === 'string';
                                    return typeof fileObjectBulk[insideKey] === 'object';
                                })
                            }
                            if (useDefaultTransformation) {
                                logger.log('info', 'Nested objects present in file ');
                                logger.log('info', 'Making defualt transformation for ' + transObjectName);
                            }
                        }
                    }
                    resolve([fileObjectBulk, useDefaultTransformation]);
                });
            }
        });
    });
}
checkFile().then((data) => {
        logger.log('debug', 'Entries of assets.json ');
        logger.log('info', '%j', data);
        return checkElement();
    }).then((anotherData) => {
        logger.log('debug', 'here ');
        return getApplicableTransformation();
    }).then((transformationData) => {
        logger.log('debug', '%j', transformationData);
        transformationData.forEach((applicableTranResBodyObject) => {
            logger.log('debug', 'Objects which are applicable for transformation : ');
            logger.log('debug', '%j', applicableTranResBodyObject);
        })
        return getFileObjectForEachTransformation(transformationData);
    }).then((fileDataWithResult) => {
        logger.log('info', 'objectssssssssss');
        logger.log('debug', '%j', fileDataWithResult);
    })
    .catch((err) => {
        logger.log('error', '%s', 'cannot find asset.json');
        logger.log('error', '%j', err);
    })