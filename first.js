var unirest = require("unirest");
var jsonfile = require('jsonfile');
const winston = require('winston');
const fs = require('fs');
const logDir = 'log';
var filePath = __dirname + '\\assets' + '\\assets.json';
var dataObject = new Date();
var hours = dataObject.getHours();
var minutes = dataObject.getMinutes();
var seconds = dataObject.getSeconds();
//timestamp = timestamp.replace(' ', '');
const tsFormat = () => (new Date()).toLocaleTimeString();
var randomName = hours + '_' + minutes + '_' + seconds;

const logger = new(winston.Logger)({
    transports: [
        // colorize the output to the console
        new(winston.transports.Console)({
            timestamp: tsFormat,
            colorize: true,
            level: 'debug'
        }),
        new(winston.transports.File)({
            //filename: logDir + '/results.log',
            filename: __dirname + '\\' + logDir + '\\results' + '_' + randomName + '.log',
            json: false,
            timestamp: tsFormat,
            level: 'debug'
        })
    ]
});
//logger.level = 'debug';
jsonfile.readFile(filePath, function(err, fileObject) {
    if (fileObject === undefined) {
        logger.log('error', '%j', err);
    } else {
        //logger.log('info', 'Entries of assets.json');
        //logger.debug('Entries of assets.json');
        logger.log('debug', 'Entries of assets.json ');
        logger.log('debug', '%j', fileObject);
        //console.log(fileObject);
        //var elementName = fileObject.elementName ? fileObject.elementName : '';
        var elementKey = fileObject.instanceKey ? fileObject.instanceKey : '';
        // var hub = fileObject.hub ? fileObject.hub : '';
        var secretUser = fileObject.secretUser ? fileObject.secretUser : '';
        //    var secretElement = fileObject.secretElement ? fileObject.secretElement : '';
        var secretOrganization = fileObject.secretOrganization ? fileObject.secretOrganization : '';
        var userOrganizationSecret = 'User ' + secretUser + ',' + ' Organization ' + secretOrganization;
        //   var elementUserOrganizationSecret = 'Element ' + secretElement + ', ' + userOrganizationSecret;
        //var randomChars = Math.random().toString(36).substring(3, 8);

        var findInstance = unirest("GET", "https://staging.cloud-elements.com/elements/api-v2/instances/" + elementKey);
        findInstance.headers({
            "authorization": userOrganizationSecret
        });
        findInstance.end(function(findInstanceRes) {

            if (findInstanceRes.error && findInstanceRes.error.status !== 404) {
                logger.log('error', 'Error while validation Please verify assets.json');
                logger.log('error', findInstanceRes.error);
                logger.log('error', findInstanceRes.body)
                throw new Error(findInstanceRes.error);
            } else {
                if (findInstanceRes.body && findInstanceRes.body.message && findInstanceRes.body.message.includes('No element')) {
                    logger.log('error', 'Error while validation Please verify assets.json');
                    logger.log('error', findInstanceRes.body)
                    logger.log('error', findInstanceRes.body.message);
                } else {
                    logger.log('info', 'assets.json is valid');
                    var elementName = findInstanceRes.body.element.key;
                    var hub = findInstanceRes.body.element.hub;
                    var secretElement = findInstanceRes.body.token;
                    var elementUserOrganizationSecret = 'Element ' + secretElement + ', ' + userOrganizationSecret;
                    var req = unirest("GET", 'https://staging.cloud-elements.com/elements/api-v2/elements/' + elementName + '/transformations');
                    req.headers({
                        "authorization": userOrganizationSecret
                    });
                    req.end(function(res) {
                        if (res.error && res.error.status !== 404) {
                            logger.log('error', 'Something went wrong while getting transformations elementName' + elementName);
                            logger.log('error', res.error);
                            throw new Error(res.error);
                        } else {
                            if (res.body && res.body.message && (res.body.message.includes('No element') || res.body.message.includes('No default transformation'))) {
                                logger.log('error', 'Something went wrong while getting transformations elementName' + elementName);
                                logger.log('error', res.body.message);
                            } else {
                                // all objects                                
                                logger.log('debug', 'Objects which are applicable for transformation : ');
                                res.body.forEach((resBodyObject) => {
                                    logger.log('debug', '%j', resBodyObject);
                                })

                                var areq = unirest("GET", 'https://staging.cloud-elements.com/elements/api-v2/instances/' + elementKey + '/transformations');
                                areq.headers({
                                    "authorization": userOrganizationSecret
                                });
                                areq.end(function(ares) {

                                    if (ares.error && ares.error.status !== 404) {
                                        logger.log('debug', 'Something went wrong while getting transformations ');
                                        logger.log('error', '%j', ares.error);
                                    } else {
                                        if (ares.body && ares.body.message && ares.body.message.includes('No element instance found with')) {
                                            logger.log('error', '%s', ares.body.message);
                                        } else {
                                            //  console.log(ares.body)
                                            // filteredArray = ares.body;
                                            // logic to filter for which Transformation is not created                                             
                                            // var existingTransArray = Object.keys(ares.body).map((key) => {
                                            //     return ares.body[key].vendorName;
                                            // })
                                            // var filteredArray = res.body.filter((resOne) => {
                                            //     return !existingTransArray.includes(resOne.vendorName);
                                            // })
                                            // objects which are not transformaed
                                            //logger.info('Objects which are not transformed : ');
                                            //logger.debug(filteredArray)

                                            //filteredArray.forEach((transObject) => {
                                            res.body.forEach((transObject) => {

                                                if (transObject) {

                                                    var useDefaultTransformation = true;
                                                    var transObjectVendorName = transObject.vendorName;
                                                    var transObjectName = transObject.name;
                                                    var fileNameBulk = __dirname + '\\elements\\' + elementName + '\\assets\\' + transObjectName + '.json';


                                                    jsonfile.readFile(fileNameBulk, function(err, fileObjectBulk) {

                                                        logger.log('info', '****************** ' + transObjectVendorName + ' ******************');

                                                        logger.log('info', 'Searching for file : ' + fileNameBulk);


                                                        if (fileObjectBulk === undefined) {
                                                            // will use default transformation
                                                            logger.log('warn', 'Cannot find : ' + transObjectName + '.json' + ' or it is invalid');
                                                            logger.log('warn', err);
                                                            logger.log('warn', 'Making defualt transformation for ' + transObjectName);
                                                        } else {
                                                            if (fileObjectBulk) {

                                                                logger.log('debug', 'file data : ');
                                                                logger.log('debug', '%j', fileObjectBulk);

                                                                if (typeof fileObjectBulk === 'object') {
                                                                    useDefaultTransformation = Object.keys(fileObjectBulk).some((insideKey) => {
                                                                        //return typeof fileNameBulk[insideKey] === 'string';
                                                                        return typeof fileObjectBulk[insideKey] === 'object';
                                                                    })

                                                                }
                                                                if (useDefaultTransformation) {

                                                                    logger.log('info', 'Nested objects present in file ');
                                                                    logger.log('info', 'Making defualt transformation for ' + transObjectName);
                                                                }
                                                            }
                                                        }
                                                        var secReq = unirest("GET", 'https://staging.cloud-elements.com/elements/api-v2/hubs/' + hub + '/objects/' + transObjectVendorName + '/metadata');
                                                        secReq.headers({
                                                            "authorization": elementUserOrganizationSecret
                                                        });
                                                        secReq.type("json");
                                                        secReq.end(function(secResObj) {
                                                            logger.log('info', '****************** ' + transObjectVendorName + ' ******************');
                                                            if (secResObj.error && secResObj.error.status !== 404) {

                                                                logger.log('fatal', "error while getting fields of original object hance cannot proceed further.");
                                                                logger.log('fatal', '%j', secResObj.body);
                                                                //console.log(secResObj.error);
                                                            } else {
                                                                var secResBody = secResObj.body;
                                                                if (secResBody && secResBody.message && secResBody.message.includes('No')) {

                                                                    logger.log('fatal', "error while getting fields of original object hance cannot proceed further.");
                                                                    logger.log('fatal', '%j', secResBody);
                                                                }
                                                                if (secResBody) {
                                                                    if (secResBody.fields) {

                                                                        var transBody = {};
                                                                        var transBodyLength = secResBody.fields.length;
                                                                        //  logger.debug('Number of fields present for ' + transObjectVendorName + ' are : ' + transBodyLength);

                                                                        var transMapBodyDefinitions = {};
                                                                        var transMapBodyTransformations = {};
                                                                        var script = {};
                                                                        // to check weather provided payload is valid or not
                                                                        if (!useDefaultTransformation) {

                                                                            var fieldsFromMetaData = secResBody.fields.map((field) => {
                                                                                return field.vendorPath;
                                                                            })
                                                                            var isSimmilerToMetaData = Object.keys(fileObjectBulk).every((field) => {
                                                                                return fieldsFromMetaData.includes(field);
                                                                            })
                                                                            if (isSimmilerToMetaData) {

                                                                                logger.log('info', 'file data is valid as per transformation need')
                                                                                logger.log('info', 'Making custom file based transformation for ' + transObjectName);
                                                                            } else {
                                                                                useDefaultTransformation = true;
                                                                                logger.log('info', 'file data is not valid as per transformation need');
                                                                                logger.log('info', 'Making default transformation for ' + transObjectName);
                                                                            }
                                                                            //isSimmilerToMetaData ? console.log('payload fields are valid') :  useDefaultTransformation = true;                                                                                           
                                                                        }

                                                                        if (!useDefaultTransformation) {

                                                                            // var fieldsFromMetaData = secResBody.fields.map((field) => {
                                                                            //         return  field.vendorPath;                                                                            
                                                                            // })
                                                                            // var isSimmilerToMetaData = Object.keys(fileObjectBulk).every((field) => {
                                                                            //     return fieldsFromMetaData.includes(field);
                                                                            // })
                                                                            transMapBodyDefinitions.fields = Object.keys(fileObjectBulk).map((field) => {

                                                                                var validMetaData = secResBody.fields.filter((secResfield) => {
                                                                                    return secResfield.vendorPath === field;
                                                                                })

                                                                                return {
                                                                                    //type: field.toLowerCase().includes('date') ?  fileObjectBulk[field] ?  field.toLowerCase(). ? 'date' : typeof fileObjectBulk[field],
                                                                                    //   type : typeof fileObjectBulk[field],
                                                                                    type: validMetaData[0].type,
                                                                                    path: field
                                                                                }
                                                                            })
                                                                            transMapBodyTransformations = Object.keys(fileObjectBulk).map((field) => {
                                                                                return {
                                                                                    path: field,
                                                                                    vendorPath: field
                                                                                }
                                                                            });
                                                                            transMapBodyTransformations[transMapBodyTransformations.length - 1].vendorPath = null;
                                                                            transMapBodyTransformations[transMapBodyTransformations.length - 1].configuration = [{
                                                                                "type": "passThrough",
                                                                                "properties": {
                                                                                    "fromVendor": false,
                                                                                    "toVendor": false
                                                                                }
                                                                            }]

                                                                            var tempPath = transMapBodyTransformations[transMapBodyTransformations.length - 1].path;
                                                                            script = {
                                                                                "body": "\r\nvar gen_obj={};\r\ngen_obj = transformedObject;\r\ngen_obj[\"" + tempPath + "\"] = originalObject[\"" + tempPath + "\"];\r\ndone(gen_obj);\r\n",
                                                                                "mimeType": "application/javascript",
                                                                                "filterEmptyResponse": false
                                                                            }
                                                                        } else {

                                                                            transBody = secResBody.fields.filter((field) => {
                                                                                //var criteria = transBodyLength < 4 ? 2 : 4;
                                                                                return !field.vendorPath.includes('.');
                                                                            });
                                                                            transBodyLength > 4 ? transBody = transBody.slice(0, 4) : transBody = transBody.slice(0, 2);

                                                                            // will use to make third request
                                                                            transMapBodyDefinitions.fields = transBody.map((field) => {
                                                                                return {
                                                                                    type: field.type,
                                                                                    path: field.vendorPath
                                                                                }
                                                                            })

                                                                            // will use to make forth request
                                                                            transMapBodyTransformations = transBody.map((field) => {
                                                                                return {
                                                                                    path: field.vendorPath,
                                                                                    vendorPath: field.vendorPath
                                                                                }
                                                                            });

                                                                            if (transMapBodyTransformations.length >= 4) {

                                                                                var tempPath = transMapBodyTransformations[transMapBodyTransformations.length - 1].path;
                                                                                var tempPath1 = transMapBodyTransformations[transMapBodyTransformations.length - 2].path

                                                                                script = {
                                                                                    "body": "\r\nvar gen_obj={};\r\ngen_obj = transformedObject;\r\ngen_obj[\"" + tempPath + "\"] = originalObject[\"" + tempPath + "\"];\r\ndone(gen_obj);\r\n\r\nvar gen_obj={};\r\ngen_obj = transformedObject;\r\ngen_obj[\"" + tempPath1 + "\"] = originalObject[\"" + tempPath1 + "\"];\r\ndone(gen_obj);\r\n",
                                                                                    "mimeType": "application/javascript",
                                                                                    "filterEmptyResponse": false
                                                                                }
                                                                                transMapBodyDefinitions.fields = transMapBodyDefinitions.fields.slice(0, transMapBodyDefinitions.fields.length - 2)
                                                                                transMapBodyTransformations = transMapBodyTransformations.slice(0, transMapBodyTransformations.length - 2);
                                                                            }

                                                                        }
                                                                        logger.log('info', 'payload to make Definition : ');
                                                                        logger.log('info', '%j', transMapBodyDefinitions)

                                                                        var thirdReq = unirest("Post", 'https://staging.cloud-elements.com/elements/api-v2/organizations/objects/' + 'generic' + transObjectVendorName + randomName + '/definitions');
                                                                        thirdReq.headers({
                                                                            "authorization": userOrganizationSecret
                                                                        });
                                                                        thirdReq.type("json");
                                                                        // create transformation
                                                                        thirdReq.send(transMapBodyDefinitions);
                                                                        thirdReq.end(function(thiredReqRes) {

                                                                            //  console.log(thiredReqRes);
                                                                            logger.log('info', '****************** ' + transObjectVendorName + ' ******************');
                                                                            if (thiredReqRes.error) {

                                                                                logger.log('info', "Error while creating transformation Definition : ");
                                                                                logger.log('info', '%j', thiredReqRes.error);
                                                                                //logger.log('info', '%j', thiredReqRes.body);
                                                                            } else {
                                                                                //logger.debug('Response for Definition creation : ');
                                                                                //logger.debug(thiredReqRes.body);                                                                     // mapping created transformation to original object
                                                                                var fourthReq = unirest("Post", 'https://staging.cloud-elements.com/elements/api-v2/instances/' + elementKey + '/transformations/' + 'generic' + transObjectVendorName + randomName);
                                                                                fourthReq.headers({
                                                                                    "authorization": userOrganizationSecret
                                                                                });
                                                                                fourthReq.type("json");
                                                                                //logger.info('mapping created transformation to original object');
                                                                                var forthPayload = {
                                                                                    "vendorName": transObjectVendorName,
                                                                                    "fields": transMapBodyTransformations,
                                                                                    "configuration": [{
                                                                                        "type": "passThrough",
                                                                                        "properties": {
                                                                                            "fromVendor": false,
                                                                                            "toVendor": false
                                                                                        }
                                                                                    }],
                                                                                    // "script":{
                                                                                    //     "body":"\r\nvar gen_obj={};\r\ngen_obj = transformedObject;\r\ngen_obj." + displayName + " = originalObject." + displayName + ";\r\ndone(gen_obj);\r\n",
                                                                                    //     "mimeType":"application/javascript",
                                                                                    //     "filterEmptyResponse":false
                                                                                    // },
                                                                                    "isLegacy": null
                                                                                }
                                                                                if (script) {
                                                                                    forthPayload.script = script;
                                                                                }


                                                                                logger.log('info', 'payload to make mapping b/w object to transformation : ');
                                                                                logger.log('debug', '%j', forthPayload);

                                                                                fourthReq.send(forthPayload);
                                                                                fourthReq.end(function(fourthReqRes) {

                                                                                    if (fourthReqRes.error && fourthReqRes.error.status !== 404) {
                                                                                        logger.log('info', '****************** ' + transObjectVendorName + ' ******************');
                                                                                        logger.log('info', 'Something went wrong while creating mappings');
                                                                                        logger.log('info', '%j', fourthReqRes.error);
                                                                                        logger.log('info', '%j', fourthReqRes.body);

                                                                                    } else {
                                                                                        if (fourthReqRes.body && fourthReqRes.body.message && fourthReqRes.body.message.includes('No element instance found with')) {
                                                                                            logger.log('info', '****************** ' + transObjectVendorName + ' ******************');
                                                                                            logger.log('info', fourthReqRes.body.message);
                                                                                        } else {
                                                                                            logger.log('info', '****************** ' + transObjectVendorName + ' ******************');
                                                                                            logger.log('debug', '%j', fourthReqRes.body);
                                                                                            logger.log('info', 'Transformation is created successfully for :' + transObjectVendorName);
                                                                                            logger.log('info', 'Transformation object name is : ' + 'generic' + transObjectVendorName + randomName);


                                                                                            var getAll = unirest("GET", 'https://staging.cloud-elements.com/elements/api-v2/hubs/' + hub + '/' + 'generic' + transObjectVendorName + randomName);
                                                                                            getAll.headers({
                                                                                                "authorization": elementUserOrganizationSecret
                                                                                            });
                                                                                            getAll.type("json");
                                                                                            getAll.end(function(getAllObj) {
                                                                                                logger.log('info', '****************** ' + transObjectVendorName + ' ******************');
                                                                                                logger.log('info', 'https://staging.cloud-elements.com/elements/api-v2/hubs/' + hub + '/' + 'generic' + transObjectVendorName + randomName);
                                                                                                logger.log('info', 'Result after testing transformation for : ' + 'generic' + transObjectVendorName + randomName);
                                                                                                if (getAllObj.error && getAllObj.error.status !== 404) {

                                                                                                    logger.log('info', "error while testing transformation objects");
                                                                                                    logger.log('info', '%j', getAllObj.body);
                                                                                                    //console.log(secResObj.error);
                                                                                                } else {
                                                                                                    var getAllObjBody = getAllObj.body;
                                                                                                    if (Array.isArray(getAllObjBody) && getAllObjBody.length >= 1) {
                                                                                                        logger.log('info', '%j', getAllObjBody[0]);
                                                                                                    } else {
                                                                                                        logger.log('info', '%j', getAllObjBody);
                                                                                                    }

                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }


                                                                                })
                                                                            }
                                                                        });
                                                                        //        }
                                                                    } else {
                                                                        //  logger.log('info', "error while getting fields of original object hance cannot proceed further.");
                                                                        logger.log('info', "response body is presnt but field not presnt");
                                                                    }
                                                                } else {
                                                                    //logger.log('info', "error while getting fields of original object hance cannot proceed further.");

                                                                    logger.log('info', "In response body is not presnt");
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                            })
                                        }
                                    }
                                })
                            }
                        }
                    });
                }
            }
        });
    }
});