To set environment for TransformationCreationTool please follow below steps 

1. Install latest version of nodejs.
2. Run "git clone https://gitlab.com/rahulguptagslab1/TranformationCreationTool.git".
3. Run "npm install" from current directory.


To run TransformationCreationTool please follow below steps 

1. First configure assets.json present inside /assets.
2. To run "node first.js" from current directory.


tool description :- 

1. Tool get the json for element to know which of the objects are applicable for transformation.
which looks like 

[
{"name":"customers","vendorName":"Customer"}
{"name":"contacts","vendorName":"Contact"}
]

2. we pick vendorName for each object from the json and search for file in \elements\{elementkeyname}\assets\{name}.json

like : c:\comapnydata\backup\automation tool\elements\netsuitefinancev2\assets\customers.json

3. then tool try to validate file.

4. validation consist of checking 'no nested object should be present in file', 'whatever the fields passed in file should be same as original fields'.

5. if file is validated we make filebased custom transformation (which is bulk with transformation).

6. if file is invalid we make default tranformation where we just map 4 fields and make tranformation.

7. if tranformation is created tool test the get for tranformation.


how to debug logs

1. search with this keword
****************** {vendorName} ******************

ex :    ****************** Vendor ******************

2. to know if any filebased custom transformation is created 
ex :    Making custom file based transformation

3. to know which tranformation is created search  keyword 
ex :    Transformation is created successfully

4. to know result of tested tranformation.
ex : Result after testing transformation for
